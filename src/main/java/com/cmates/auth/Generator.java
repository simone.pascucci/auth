/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cmates.auth;

import com.cmates.rest.filters.authorization.utils.EncryptionUtil;

/**
 *
 * @author simonepascucci
 */
public class Generator {

    public static void main(String [ ] args) throws Exception {
        Generator.testCaculateHMAC();
        BatchProcessor bp = new BatchProcessor();
        bp.processBatch();
    }

    public static void testCaculateHMAC() throws Exception {
        String uri = "/schools/15613381/classes/1999/affiliations";
        String clientId = "1100003"; //TGC
        String secretKey = "QWxvaG9tb3Jh"; //TCG
        int timestamp = (int) ((System.currentTimeMillis()) / 1000);
        System.out.println("timestamp : " + timestamp);

        String hmacHash = EncryptionUtil.calculateHMAC(uri + String.valueOf(timestamp), secretKey);
        System.out.println("CM-API " + clientId + ":" + timestamp + ":" + hmacHash);
    }
}
