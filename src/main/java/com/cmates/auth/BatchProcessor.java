package com.cmates.auth;

import com.cmates.rest.filters.authorization.utils.EncryptionUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvException;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.security.SignatureException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author simone
 */
public class BatchProcessor {

    private final String clientId = "1000020";
    private final String secretKey = "";
    private final String baseUri = "https://api.classmates.com";
    //private final String baseUri = "https://api.nonprod.cmates.com";

    private List<String[]> parseCsv() {
        List<String[]> r = null;
        try (CSVReader reader = new CSVReader(new FileReader(getClass().getClassLoader().getResource("site-db-prod-prov-entire.csv").getFile()))) {
            r = reader.readAll();
            r.forEach(x -> System.out.println(Arrays.toString(x)));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(BatchProcessor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException | CsvException ex) {
            Logger.getLogger(BatchProcessor.class.getName()).log(Level.SEVERE, null, ex);
        }
        return r;
    }

    private String getAuthHeader(String uri) throws SignatureException {
        int timestamp = (int) ((System.currentTimeMillis()) / 1000);
        //System.out.println("timestamp : " + timestamp);

        String hmacHash = EncryptionUtil.calculateHMAC(uri + String.valueOf(timestamp), secretKey);
        //System.out.println("CM-API " + clientId + ":" + timestamp + ":" + hmacHash);

        return "CM-API " + clientId + ":" + timestamp + ":" + hmacHash;
    }

    private String getEtag(String uri) throws SignatureException {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        requestHeaders.add("Authorization", getAuthHeader(uri));
        ResponseEntity<String> response;
        String url = baseUri + uri;
        try {
            response = restTemplate.exchange(
                    url,
                    HttpMethod.GET,
                    new HttpEntity<>(requestHeaders),
                    String.class);
            if (!response.getStatusCode().equals(HttpStatus.OK)) {
                Logger.getLogger(BatchProcessor.class.getName()).log(Level.SEVERE, "Failed getting id {0}", uri);
                throw new RuntimeException();
            }
        }catch (HttpClientErrorException ex){
            if (ex.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                Logger.getLogger(BatchProcessor.class.getName()).log(Level.WARNING, "Failed getting id " + uri + " with 404.", ex);
                return null;
            }
            Logger.getLogger(BatchProcessor.class.getName()).log(Level.SEVERE, "Failed getting id " + uri, ex);
            throw ex;
        } catch (Exception ex) {
            Logger.getLogger(BatchProcessor.class.getName()).log(Level.SEVERE, "Failed getting id " + uri, ex);
            throw ex;
        }
        return response.getHeaders().getETag();
    }

    public void processBatch() throws SignatureException, JsonProcessingException {
        List<String[]> ids = parseCsv();
        RestTemplate restTemplate = new RestTemplate();
        String uri = "/provisions/roles/";
        Set<String> processedIds = new HashSet<>();
        if (!CollectionUtils.isEmpty(ids)) {
            boolean first = true;
            for (String[] id : ids) {
                if (first || processedIds.contains(id[0])) {
                    first = false;
                    continue;
                }
                Logger.getLogger(BatchProcessor.class.getName()).log(Level.INFO, "Processing id {0}", id[0]);
                processedIds.add(id[0]);
                HttpHeaders requestHeaders = new HttpHeaders();
                requestHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
                requestHeaders.add("Content-type", "application/json");
                requestHeaders.add("Authorization", getAuthHeader(uri + id[0]));
                String eTag = getEtag(uri + id[0]);
                if (eTag == null) {
                    continue;
                }
                requestHeaders.add("If-Match", eTag);
                requestHeaders.add("X-Session-ID", "xxxxxxxxx");
                
                HashMap<String, String> jsonBody = new HashMap<>();
                jsonBody.put("revokedDate", new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").format(new Date()));
                
                ObjectMapper mapper = new ObjectMapper();
                String revokeBody = mapper.writeValueAsString(jsonBody);
                
                HttpEntity<String> request = 
                		new HttpEntity<String>(revokeBody, requestHeaders);

                ResponseEntity<String> response;
                String url = baseUri + uri + id[0];
                try {
                    response = restTemplate.exchange(
                            url,
                            HttpMethod.POST,
                            request,
                            String.class);
                    if (!response.getStatusCode().equals(HttpStatus.OK)) {
                        Logger.getLogger(BatchProcessor.class.getName()).log(Level.SEVERE, "Failed updating id {0}", id[0]);
                        return;
                    }
                } catch (Exception ex) {
                    Logger.getLogger(BatchProcessor.class.getName()).log(Level.SEVERE, "Failed updating id " + id[0], ex);
                    return;
                }
            }
        }
    }
}